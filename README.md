### Prerequisites
* [Docker](https://docs.docker.com/install/#supported-platforms)
* [Git](https://git-scm.com/downloads)
* [Azure Account](https://azure.microsoft.com/en-us/offers/ms-azr-0044p/)

### Run this sequence to spin up an Azure IOT Hub using Docker!
##### 1. clone the repo
`git clone https://sp1cefl0w@bitbucket.org/sp1cefl0w/bentobox.git`
##### 2. cd into the repo
`cd bentobox`
##### 3. spin up the azure-cli container
`docker-compose up -d`
##### 4. wreck shop
`docker exec -ti bentobox sh -c "az extension add --name azure-cli-iot-ext && az login && az group create --name bentobox --location westus && az iot hub create --resource-group bentobox --name unagi --sku S1"`

*Note 1 - `az login` will prompt you to open your browser and login with the code displayed in the prompt*

*Note 2 - modify the shell commands here as needed (i.e. remove the `az group create` command and change the `--name` parameter in the `az iot hub create` command to use an existing resource group)*